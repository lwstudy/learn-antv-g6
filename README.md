# learn-G6
> 使用脚手架（自动生成接口、路由）实现一些G6在vue2中的实例写法
已实现ER等
#### 介绍
antv/G6学习与实例
> 为什么选择G6呢，因为X6底层是基于svg，所以当节点多了（100以上基本就要画图很久了，并且难以操作）
所以以为需要大数据渲染节点，且操作并不那么复杂，所以改成了基于canvas的antv/G6


### ER图---完善为db数据库图表
![avatar](/img/dber.png)
### 思维导图---思维导图子节点根据类型变换颜色
![avatar](/img/swdt.png)
## 项目安装启动
```
npm install
# 或者yarn
```

### 编译运行开发环境
```
npm run serve
```

### 编译部署生产环境
```
npm run build
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
